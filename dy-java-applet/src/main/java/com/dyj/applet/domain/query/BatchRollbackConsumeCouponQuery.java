package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.UserInfoQuery;

import java.util.List;

/**
 * 用户撤销核销券请求值
 */
public class BatchRollbackConsumeCouponQuery extends UserInfoQuery {

    /**
     *
     */
    private String app_id;
    /**
     * 外部核销单号 选填
     */
    private String consume_out_no;

    /**
     *  选填
     */
    private String order_id;
    /**
     *  选填
     */
    private String rollback_consume_out_no;
    /**
     *  选填
     */
    private Long rollback_consume_time;
    /**
     *  选填
     */
    private List<String> coupon_id_list;

    public String getApp_id() {
        return app_id;
    }

    public BatchRollbackConsumeCouponQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getConsume_out_no() {
        return consume_out_no;
    }

    public BatchRollbackConsumeCouponQuery setConsume_out_no(String consume_out_no) {
        this.consume_out_no = consume_out_no;
        return this;
    }

    public String getOrder_id() {
        return order_id;
    }

    public BatchRollbackConsumeCouponQuery setOrder_id(String order_id) {
        this.order_id = order_id;
        return this;
    }

    public String getRollback_consume_out_no() {
        return rollback_consume_out_no;
    }

    public BatchRollbackConsumeCouponQuery setRollback_consume_out_no(String rollback_consume_out_no) {
        this.rollback_consume_out_no = rollback_consume_out_no;
        return this;
    }

    public Long getRollback_consume_time() {
        return rollback_consume_time;
    }

    public BatchRollbackConsumeCouponQuery setRollback_consume_time(Long rollback_consume_time) {
        this.rollback_consume_time = rollback_consume_time;
        return this;
    }

    public List<String> getCoupon_id_list() {
        return coupon_id_list;
    }

    public BatchRollbackConsumeCouponQuery setCoupon_id_list(List<String> coupon_id_list) {
        this.coupon_id_list = coupon_id_list;
        return this;
    }

    public static BatchRollbackConsumeCouponQueryBuilder builder(){
        return new BatchRollbackConsumeCouponQueryBuilder();
    }

    public static final class BatchRollbackConsumeCouponQueryBuilder {
        private String app_id;
        private String consume_out_no;
        private String order_id;
        private String rollback_consume_out_no;
        private Long rollback_consume_time;
        private List<String> coupon_id_list;
        private String open_id;
        private Integer tenantId;
        private String clientKey;

        private BatchRollbackConsumeCouponQueryBuilder() {
        }

        public static BatchRollbackConsumeCouponQueryBuilder aBatchRollbackConsumeCouponQuery() {
            return new BatchRollbackConsumeCouponQueryBuilder();
        }

        public BatchRollbackConsumeCouponQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder consumeOutNo(String consumeOutNo) {
            this.consume_out_no = consumeOutNo;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder orderId(String orderId) {
            this.order_id = orderId;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder rollbackConsumeOutNo(String rollbackConsumeOutNo) {
            this.rollback_consume_out_no = rollbackConsumeOutNo;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder rollbackConsumeTime(Long rollbackConsumeTime) {
            this.rollback_consume_time = rollbackConsumeTime;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder couponIdList(List<String> couponIdList) {
            this.coupon_id_list = couponIdList;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder openId(String openId) {
            this.open_id = openId;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public BatchRollbackConsumeCouponQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public BatchRollbackConsumeCouponQuery build() {
            BatchRollbackConsumeCouponQuery batchRollbackConsumeCouponQuery = new BatchRollbackConsumeCouponQuery();
            batchRollbackConsumeCouponQuery.setApp_id(app_id);
            batchRollbackConsumeCouponQuery.setConsume_out_no(consume_out_no);
            batchRollbackConsumeCouponQuery.setOrder_id(order_id);
            batchRollbackConsumeCouponQuery.setRollback_consume_out_no(rollback_consume_out_no);
            batchRollbackConsumeCouponQuery.setRollback_consume_time(rollback_consume_time);
            batchRollbackConsumeCouponQuery.setCoupon_id_list(coupon_id_list);
            batchRollbackConsumeCouponQuery.setOpen_id(open_id);
            batchRollbackConsumeCouponQuery.setTenantId(tenantId);
            batchRollbackConsumeCouponQuery.setClientKey(clientKey);
            return batchRollbackConsumeCouponQuery;
        }
    }
}
