package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.ProductAttr;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 11:29
 **/
public class GoodsTemplateVo {

    /**
     * 商品属性列表
     */
    private List<ProductAttr> product_attrs;
    /**
     * SKU属性列表
     */
    private List<ProductAttr> sku_attrs;

    public List<ProductAttr> getProduct_attrs() {
        return product_attrs;
    }

    public void setProduct_attrs(List<ProductAttr> product_attrs) {
        this.product_attrs = product_attrs;
    }

    public List<ProductAttr> getSku_attrs() {
        return sku_attrs;
    }

    public void setSku_attrs(List<ProductAttr> sku_attrs) {
        this.sku_attrs = sku_attrs;
    }
}
