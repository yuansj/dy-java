package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.SupplierMatchStatus;
import com.dyj.common.domain.vo.BaseVo;

import java.util.List;

/**
 * @author danmo
 * @date 2024-04-28 16:12
 **/
public class SupplierTaskStatusVo extends BaseVo {

    private List<SupplierMatchStatus> match_result_list;

    public List<SupplierMatchStatus> getMatch_result_list() {
        return match_result_list;
    }

    public void setMatch_result_list(List<SupplierMatchStatus> match_result_list) {
        this.match_result_list = match_result_list;
    }
}
