package com.dyj.applet.domain;

/**
 * 创建营销活动-分享裂变玩法
 */
public class CreatePromotionActivityImActivityShareFissionConfig {


    /**
     * <p>奖励券的模板ID</p> 选填
     */
    private Long award_coupon_id;
    /**
     * <p>奖励的券数量「代表完成分享任务后可以获得X张奖励券，当前仅能为1」</p> 选填
     */
    private Integer award_coupon_num;
    /**
     * 奖励类型
     */
    private Integer award_type;

    public Long getAward_coupon_id() {
        return award_coupon_id;
    }

    public CreatePromotionActivityImActivityShareFissionConfig setAward_coupon_id(Long award_coupon_id) {
        this.award_coupon_id = award_coupon_id;
        return this;
    }

    public Integer getAward_coupon_num() {
        return award_coupon_num;
    }

    public CreatePromotionActivityImActivityShareFissionConfig setAward_coupon_num(Integer award_coupon_num) {
        this.award_coupon_num = award_coupon_num;
        return this;
    }

    public Integer getAward_type() {
        return award_type;
    }

    public CreatePromotionActivityImActivityShareFissionConfig setAward_type(Integer award_type) {
        this.award_type = award_type;
        return this;
    }

    public static CreatePromotionActivityImActivityShareFissionConfigBuilder builder(){
        return new CreatePromotionActivityImActivityShareFissionConfigBuilder();
    }

    public static final class CreatePromotionActivityImActivityShareFissionConfigBuilder {
        private Long award_coupon_id;
        private Integer award_coupon_num;
        private Integer award_type;

        private CreatePromotionActivityImActivityShareFissionConfigBuilder() {
        }

        public static CreatePromotionActivityImActivityShareFissionConfigBuilder aCreatePromotionActivityImActivityShareFissionConfig() {
            return new CreatePromotionActivityImActivityShareFissionConfigBuilder();
        }

        public CreatePromotionActivityImActivityShareFissionConfigBuilder awardCouponId(Long awardCouponId) {
            this.award_coupon_id = awardCouponId;
            return this;
        }

        public CreatePromotionActivityImActivityShareFissionConfigBuilder awardCouponNum(Integer awardCouponNum) {
            this.award_coupon_num = awardCouponNum;
            return this;
        }

        public CreatePromotionActivityImActivityShareFissionConfigBuilder awardType(Integer awardType) {
            this.award_type = awardType;
            return this;
        }

        public CreatePromotionActivityImActivityShareFissionConfig build() {
            CreatePromotionActivityImActivityShareFissionConfig createPromotionActivityImActivityShareFissionConfig = new CreatePromotionActivityImActivityShareFissionConfig();
            createPromotionActivityImActivityShareFissionConfig.setAward_coupon_id(award_coupon_id);
            createPromotionActivityImActivityShareFissionConfig.setAward_coupon_num(award_coupon_num);
            createPromotionActivityImActivityShareFissionConfig.setAward_type(award_type);
            return createPromotionActivityImActivityShareFissionConfig;
        }
    }
}
