package com.dyj.applet.handler;

import com.dtflys.forest.annotation.JSONBody;
import com.dyj.applet.domain.CreatePromotionActivityV2;
import com.dyj.applet.domain.ModifyPromotionActivity;
import com.dyj.applet.domain.TalentCouponLimit;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.ConsumeCouponIdListVo;
import com.dyj.applet.domain.vo.CreatePromotionActivityVo;
import com.dyj.applet.domain.vo.QueryCouponReceiveInfoVo;
import com.dyj.applet.domain.vo.QueryPromotionActivityV2Vo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * 小程序券
 */
public class PromotionCouponHandler extends AbstractAppletHandler {
    public PromotionCouponHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 查询用户可用券信息
     * @param body 查询用户可用券信息请求值
     * @return
     */
    public DySimpleResult<QueryCouponReceiveInfoVo> queryCouponReceiveInfo(QueryCouponReceiveInfoQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().queryCouponReceiveInfo(body);
    }

    /**
     * 用户撤销核销券
     * @param body 用户撤销核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchRollbackConsumeCoupon(BatchRollbackConsumeCouponQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().batchRollbackConsumeCoupon(body);
    }


    /**
     * 复访营销活动实时圈选用户
     * @param body 复访营销活动实时圈选用户请求值
     * @return
     */
    public DySimpleResult<?> bindUserToSidebarActivity(BindUserToSidebarActivityQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().bindUserToSidebarActivity(body);
    }


    /**
     * 用户核销券
     * @param body 用户核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchConsumeCoupon(BatchConsumeCouponQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().batchConsumeCoupon(body);
    }

    /**
     * 查询主播发券配置信息
     * @param body 查询主播发券配置信息请求值
     * @return
     */
    public DySimpleResult<TalentCouponLimit> queryTalentCouponLimit(QueryTalentCouponLimitQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().queryTalentCouponLimit(body);
    }

    /**
     * 修改主播发券权限状态
     * @param body 修改主播发券权限状态请求值
     * @return
     */
    public DySimpleResult<?> updateTalentCouponStatus(UpdateTalentCouponStatusQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().updateTalentCouponStatus(body);
    }

    /**
     * 更新主播发券库存上限
     * @param body 更新主播发券库存上限请求值
     * @return
     */
    public DySimpleResult<?> updateTalentCouponStock(UpdateTalentCouponStockQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().updateTalentCouponStock(body);
    }

    /**
     * 主播发券权限配置
     * @param body 主播发券权限配置请求值
     * @return
     */
    public DySimpleResult<?> setTalentCouponApi(SetTalentCouponApiQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().setTalentCouponApi(body);
    }

    /**
     * 创建营销活动
     * @param body 创建营销活动请求值
     * @return
     */
    public DySimpleResult<CreatePromotionActivityVo> createPromotionActivityV2(PromotionActivityQuery<CreatePromotionActivityV2> body){
        baseQuery(body);
        return getPromotionCouponClient().createPromotionActivityV2(body);
    }


    /**
     * 修改营销活动
     * @param body 修改营销活动请求值
     * @return
     */
    public DySimpleResult<?> modifyPromotionActivityV2(PromotionActivityQuery<ModifyPromotionActivity> body){
        baseQuery(body);
        return getPromotionCouponClient().modifyPromotionActivityV2(body);
    }

    /**
     * 查询营销活动
     * @param body 查询营销活动请求值
     * @return
     */
    public QueryPromotionActivityV2Vo queryPromotionActivityV2(QueryPromotionActivityV2Query body){
        baseQuery(body);
        return getPromotionCouponClient().queryPromotionActivityV2(body);
    }


    /**
     * 修改营销活动状态
     * @param body 修改营销活动状态请求值
     * @return
     */
    public DySimpleResult<?> updatePromotionActivityStatusV2(UpdatePromotionActivityStatusV2Query body) {
        baseQuery(body);
        return getPromotionCouponClient().updatePromotionActivityStatusV2(body);
    }
}
